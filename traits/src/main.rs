/*
    A "Trait" in Rust is a way to tell the Rust compiler about functionality a particular type has and can share with other types.

    Traits are similar to what other languages call "interfaces". There are some differences, though.
*/

// Inside the curly braces block of a trait, we declare the method signatures that describe the behaviours
// of the types that implement this trait.
pub trait Summary {
    // After each method, we use the semicolon instead of a definition.
    // Custom behaviour must be implemented by each type implementing this Trait.
    fn summarize(&self) -> String;

    // Traits allow us to define default implementations for methods.
    fn summarize_complex(&self) -> String {
        String::from("(Read more...)")
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

fn main() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from("of course, as you probably already know, people"),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());

    // calling unimplementing summarize_complex
    println!("complex summary: {}", tweet.summarize_complex());
}

// Functions can take specific Traits as parameters.
// Instead of a concrete type, we use the "impl" keyword followed by the Trait
pub fn notify(item: impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

// The above definition is actually syntactic sugar for this longer form
// What we're really doing is defining a generic function with "Trait Bound Syntax",
// Where we specify the traits that type T is allowed to be for the function to work properly.
pub fn notify_long_form<T: Summary>(item: T) {
    println!("Breaking news! {}", item.summarize());
}
