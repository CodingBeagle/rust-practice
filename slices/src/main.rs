/*
    Slices is a data type which has no ownership, like references.
    A slice is a type of reference which lets you refer to a contiguous sequence of elements
    in a collection, rather than having to reference the whole collection itself.
*/

fn main() {
    let mut hello_world = String::from("Hello World");

    let first_word = first_word(&hello_world);

    // This line is not allowed!
    // This is because .clear() takes a MUTABLE reference to itself. However, our first_word
    // variable currently holds an immutable string reference slice to the same data.
    // As per the Rust rules, you cannot have both an immutable reference and a mutable reference
    // to the same data in the same scope.
    // hello_world.clear();

    println!("The first word is {}", first_word);

    // String literals are stored inside the binary.
    // They are actually of type "&str", meaning they are a string slice pointing to that specific
    // part of the binary.
    // This is also why string literals are immutable. "&str" is an immutable reference.
}

fn first_word(some_string: &String) -> &str {
    let string_as_bytes = some_string.as_bytes();

    for (i, &item) in string_as_bytes.iter().enumerate() {
        if item == b' ' {
            return &some_string[0..i];
        }
    }

    &some_string[..]
}