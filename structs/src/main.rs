fn main() {
    let tucker = Dog {
        name: String::from("tucker"),
        age: 5,
        has_home: true,
    };

    println!("Dog's name is {}!", tucker.name);
    println!("{} is {} years old!", tucker.name,tucker.age);
    println!("{} {} a home.", tucker.name, "");
}

struct Dog {
    name: String,
    age: u32,
    has_home: bool
}