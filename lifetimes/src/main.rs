fn main() {
    // This will work
    // Notice that the overlapping lifetime of string1 and string2 is "a" as seen below.
    // This is the lifetime where these two variables overlap
    // Notice that this code works because our return value "result" is used WITHIN the scope.
    let string1 = String::from("long string is long");

    { // ----------------------------------------------------------- a
        let string2 = String::from("xyz");                         //| 
        let result = longest(string1.as_str(), string2.as_str());  //|
        println!("The longest string is {}", result);              //|
    } // ------------------------------------------------------------v


    /*
        This code here will NOT work. Notice that result is defined in the outer scope,
        but "notworkstring2" is defined in the inner scope.

        the concrete lifetime will still be the overlapping lifetime of all references involved,
        which is "notworkstring2". For this code to work, "notworkstring2" would have to be moved out to the
        outer scope!
    */

    // let notworkstring1 = String::from("long string is long");
    // let result;
    // {
    //    let notworkstring2 = String::from("xyz");
    //    result = longest(notworkstring1.as_str(), notworkstring2.as_str());
    // }

    // println!("The longest string is {}", result);
}

/*
    This function will not actually compile.

    We take in two string slice references. 
    We return a reference to one of those string slice references.

    We need to specify a generic lifetime parameter for the return type.

    This is because Rust can't tell whether the reference being returned refers to x or y.
    We can't either due to the if statement. It depends on the values being passed in.
    We also don't know the concrete lifetimes of the references passed into the function,
    So no lifetimes can be analyzed.

    Rust doesn't know how the lifetimes of X and Y relate to the lifetime of the return value.
    So we have to add generic lifetime parameters to define the relationship between the references.
*/

//fn longest(x: &str, y: &str) -> &str {
//   if (x.len() > y.len()) {
//        x
//    } else {
//        y
//    }
//}

// Here, we are expressing that all the references in the parameters and the return value must have the same lifetime.
// If they didn't, we could end up in a situation where we returned a reference to parameter "y", which then went out of scope right after.
// Then some variable containing our return value would have a dangling reference to variable "y".
// This way, we're basically saying "The lifetime of this return value has to be usable for the entire duration of parameter x and y's lifetimes."
// Now, the borrow checker will reject any value that do not adhere to these constraints.
// When we actually run this code, generic lifetime 'a will be substituted by the concrete lifetime that is equal to the smaller of the lifetimes of
// x and y. Because the return value also has the generic lifetime 'a, the return value must be valid for the length of the smaller of the lifetimes x and y.
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if (x.len() > y.len()) {
        x
    } else {
        y
    }
}