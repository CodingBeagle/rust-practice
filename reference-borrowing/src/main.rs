/*
    The rules of References
    - At any given time, you can have EITHER one mutable reference OR any number of immutable
      References.
    - References must always be valid.
*/

fn main() {
    let mut s1 = String::from("hello");

    // Here, we use the "&" syntax to create a REFERENCE to our s1 variable.
    let len = calculate_length(&s1);
    println!("The length of '{}' is {}.", s1, len);

    // You can only have ONE mutable reference to a piece of data in a particular scope.
    // Thus, you would not be able to do this:
    // let r1 = &mut s1;
    // let r2 = &mut s1;
    // println!("{} {}", r1, r2);

    // This is one of the things people new to Rust will struggle with.
    // What this constraint does is that Rust can help prevent data races at compile time.
    // A data race (a race condition) happens when these three behaviours occur:
    // - Two or more pointers access the same data at the same time
    // - At least one of the pointers is being used to write to the dat
    // - There's no mechanism being used to synchronize access to the data

    // Likewise, you can also NOT have an immutable reference whilst you have a mutable reference
    // In the same scope.

    // Here, we use the "&mut" syntax to create a mutable reference
    change(&mut s1);
    println!("The value of s1 is now {}", s1);
}

// The "&" is used to indicate a reference to data.
// References allow you to refer to a value WITHOUT taking ownership of it.
// In Rust, the word "borrowing" is used for having a reference to something.
fn calculate_length(s: &String) -> usize {
    s.len()
} // Here, the s variable goes out of scope. However, it's a reference, so it does not have
  // ownership of what it refers to. Therefore, nothing happens (something like Drop() isn't called).

// Mutable references allow code to change the value referred to.
// HOWEVER, they have a big restriction which immutable references do not:
// You can have ONLY one mutable reference to a particular piece of data in a particular scope.
fn change(some_string: &mut String) {
    some_string.push_str(", world!");
}