use std::fs::File;
use std::io::ErrorKind;
use std::io;
use std::io::Read;

fn main() {
    // the File::open function returns a "Result<T, E>" enum.
    // This enum contains two generic parameters.
    // The type "T" returned if everything went OK, and the type "E", which is an error in case an error happened.
    let f = std::fs::File::open("hey.txt");

    // We can use a match expression to take different routes depending on if the result is Ok and some kind of Err (error).
    let f = match f {
        Ok(file) => file,
        // The error returned inside the "Err" variant of the Result object is of type "io::Error"
        // This struct is provided by the STD. It has a method "kind" which returns the kind of error through the "io::ErrorKind" value.
        // io::ErrorKind is also provided by the standard library.
        Err(error) => match error.kind() {
            // Here, we say that if the kind of error is "NotFound", we try to create the file.
            // Now, attempting to create a file also returns an io::Result object, which we immediately match on.
            // If everything is well, we return the file handle to the variable "f", otherwise we panic
            ErrorKind::NotFound => match File::create("hey.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            // In case the error when trying to open the file was any other error than "NotFound", we simply panic.
            other_error => panic!("Problem opening the file: {:?}", other_error),
        },
    };

    // The Result<T, E> type has many shortcut helper methods.
    // One of these is "unwrap". Unwrap will return the value if Result was OK, otherwise it will panic with the error message.
    // let my_awesome_file = File::open("bla.txt").unwrap();

    // Another helper method is "expect", which does the same as "unwrap", only it allows us to choose the error message
    let my_awesome_file_2 = File::open("haha.txt").expect("Failed to open file haha.txt");
}

fn read_username_from_file() -> Result<String, io::Error> {
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        // Here, we propagate the error back to the calling code.
        // Sometimes, it can be helpful to propagate an error back to calling code, as they might have
        // A better context to determine what should be done.
        Err(e) => return Err(e),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

fn read_username_from_file_shorter() -> Result<String, io::Error> {
    // Actually, propagating errors in Rust is such a common pattern that there's
    // A shorthand syntax for doing so. The "?" operator.
    // The "?" operator placed AFTER a Result value is defined to work the same way as the match expression
    // In the longer version of this function seen above!
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;

    Ok(s)
}

fn read_username_from_file_even_shorter() -> Result<String, io::Error> {
    let mut s = String::new();
    File::open("hello.txt")?.read_to_string(&mut s)?;
    Ok(s)
}