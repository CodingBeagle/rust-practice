/*
    As many other programming languages, Generic Types are a way to abstract away the concrete type being used in a piece
    of code. Much the same way that, say, variables of a specific type is a way to abstract away the concrete value.
    Generics just go one step further and abstract away the concrete type of that data as well.
*/

// In Rust, Generic Types can be used on Structs
// The syntax <> is used to define the name of the type parameter. This type parameter can then
// Be used within the struct.
struct Point<T> {
    x: T,
    y: T
}

// In rust, you can implement methods on structs, and use generic types in their definitions, too.
// Notice we have to specify <T> on "impl" to indicate that we're implementing methods on 
// the generic point type Point<T>
impl<T> Point<T> {
    // Here, I define a method named "get_x" on Point<T>, which returns a reference to the data 
    // In the field x.
    fn get_x(&self) -> &T {
        &self.x
    }

    fn get_y(&self) -> &T {
        &self.y
    }
}

// This is because you can actually implement methods for specific concrete types of a generic struct!
impl Point<i32> {
    fn add(&self) -> i32 {
        self.x + self.y
    }
}

// It is also possible to define multiple generic types, for Structs as well
struct ComplexPoint<T, U> {
    x: T,
    y: U
}

// Generics can also be used with the variants of Enums!
// Here, we have a generic enum for type T and E
// Variant "ItsCool" holds the type of T
// And variant "NotCool" holds the type of E.
// This is how types like "Option" and "Result" enums are defined in the standard library.
enum MyResult<T, E> {
    ItsCool(T),
    NotCool(E)
}

fn main() {
    let my_awesome_struct = Point { x: 5, y: 5 };
    println!("X value is: {}, Y value is: {}", my_awesome_struct.get_x(), my_awesome_struct.get_y());
    println!("Values added together are: {}", my_awesome_struct.add());


    let my_awesome_complex_struct = ComplexPoint { x: 5, y: String::from("lol!!") };
    println!("X value is: {}, Y value is: {}", my_awesome_complex_struct.x, my_awesome_complex_struct.y);

    //let number_list = vec![34, 50, 25, 100, 65];
    //println!("The largest number is {}", largest(&number_list));
}


/*
fn largest<T>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    largest
}*/
